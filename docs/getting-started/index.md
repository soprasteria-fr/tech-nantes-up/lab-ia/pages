Let's go! On démarre notre "RAG against the Machine" maintenant!

Une fois que vous êtes connecté à votre espace VS Code, pensez à bien "trust" le répertoire `/workdir`.

**Il est possible qu'il y ait une erreur de sécurité à la première connexion. En effet, le challenge HTTP-01 de l'environnement pour sécuriser l'ingress peut prendre un certain temps.**

Ouvrez ensuite une console en cliquant sur le hamburger en haut à gauche de VS Code, puis `Terminal` > `New Terminal`. Il va y avoir quelques commandes à entrer pour démarrer (pensez à autoriser l'accès au presse-papiers dans votre navigateur si on vous le demande).

## Cloner le dépôt de l'énoncé

Commencez par cloner le dépôt de l'énoncé avec la ligne de commande suivante :

```bash
git clone https://gitlab.com/soprasteria-fr/tech-nantes-up/lab-ia/codelab-enonce.git
```

## Cloner le dépôt des corrections

En parallèle, si vous en avez besoin, vous pouvez aussi cloner le dépôt des corrections :

```bash
git clone https://gitlab.com/soprasteria-fr/tech-nantes-up/lab-ia/codelab-corrige.git
```

(M'enfin, ça reste moins fun que de suivre nos instructions pas du tout bancales 😇).

## Installer les dépendances

Déplacez-vous dans le dossier de l'énoncé et installez les dépendances :

```bash
pip install -r requirements.txt
```

On est fin prêt pour démarrer à coder 🚀.
