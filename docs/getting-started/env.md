Le Lab utilise des API fournies par OVH et Azure.

Pour permettre l'appel à ces services, il est nécessaire d'initialiser un fichier contenant notamment des secrets pour appeler ces APIs de manière sécurisée.

## Copier le fichier `.env.example`

```bash
cp .env.example .env
```

## Comprendre les variables

Le fichier `.env` comprend les variables utiles pour faire fonctionner le lab.

Seules deux variables sont volontairement vides et correspondent à des secrets que tu devras remplir.

* `DOCUMENTS_DIR` : chemin vers le dossier contenant les documents à indexer
* `DATABASE_DIR` : chemin vers le dossier contenant la base de données ChromaDB
* `OVH_AI_ENDPOINTS_ACCESS_TOKEN` : clé d'accès à l'API OVH AI Endpoints. Utilise le token retourné par la page des endpoints IA d'OVH !
* `OPENAI_API_VERSION` : version de l'API OpenAI à utiliser
* `AZURE_OPENAI_ENDPOINT` : URL de l'API Azure OpenAI
* `AZURE_OPENAI_API_KEY` : clé d'API Azure OpenAI. Cette clé te sera fournie plus tard dans le lab, pas de panique 😎

Si vous ne l'avez pas déjà fait, pensez à récupérer votre jeton d'accès [OVH_AI_ENDPOINTS_ACCESS_TOKEN](https://endpoints.ai.cloud.ovh.net/).
