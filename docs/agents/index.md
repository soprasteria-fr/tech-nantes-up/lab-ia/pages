Dans cette dernière partie, nous allons voir comment utiliser des agents et faire du function calling !

**La suite de ce lab sera réalisée sous OpenAI. En effet, ceux-ci disposent d'API capables de faire de l'invocation de fonctions.**

Pourquoi faire du function calling ?

Simplement parce que cela permet de mobiliser des outils déterministes ou des contenus à jour pour répondre aux sollicitations des utilisateurs. En effet, les LLMs sont, par exemple, particulièrement mauvais pour effectuer des calculs ou pour répondre à un fait très récent. Le function calling permet donc de pallier cela en mobilisant des outils.

Le LLM va alors devoir faire le choix de l'outil adéquat, générer la signature de méthode avec les paramètres. Nous nous chargeons ensuite de l'exécution puis de lui réinjecter le résultat pour inférer la réponse.

Allez, on a assez parlé, passons à l'action !

Pour suivre cette partie, ouvrez le fichier :

* Prompt en mode Agent : `prompt_agent.py`

## Initialisation

Récupèrez la clé d'API sur ce pastebin : [https://pastebin.com/tvFzVsFa](https://pastebin.com/tvFzVsFa).

Commencez par éditer votre fichier `.env` pour renseigner la clé d'API OpenAI :

```sh
OPENAI_API_KEY="<clé fournie par les organisateurs>"
```

Voici ensuite le code minimal pour démarrer avec les agents :

```python
import logging

from langchain import hub
from langchain.agents import AgentExecutor, create_openai_functions_agent
from langchain.chains.llm_math.base import LLMMathChain
from langchain.tools import Tool
from langchain_openai import ChatOpenAI as Chat

import constants

# Configure le logger
logging.basicConfig(
    format="%(levelname)s: %(message)s", encoding="utf-8", level=logging.ERROR
)

# Instanciation du chat
_CHAT = Chat(model_name="gpt-4o", temperature=constants.TEMPERATURE)
```

**Merci d'utiliser les services mis à disposition uniquement pour l'exécution du Lab ❤**

## Créer un outil

Avant de créer un agent, il est nécessaire de déclarer les outils dont il va avoir besoin.

Un outil se déclare de la manière suivante :

```python
_TOOLS = [
    Tool.from_function(
        func=LLMMathChain.from_llm(llm=_CHAT, verbose=True).run,
        name="Calculator",
        description="Useful when you need to answer questions about math",
        handle_tool_error=True,
    ),
]
```

Analysons un peu ses paramètres :

* `func` : la fonction à appeler. Ici, nous appelons une fonction intégrée dans langchain pour faire des calculs mathématiques.
* `name` : le nom de l'outil, il sera utilisé pour appeler l'outil.
* `description` : la description de l'outil, elle sera utilisée pour aider l'agent à comprendre quand il doit mobiliser l'outil.

Comme vous l'aurez compris, notre premier outil et l'agent qui pourra l'utiliser permettront d'effectuer des calculs avec un évaluateur Python.

## Initialiser son agent

Pour initialiser l'agent, nous allons directement lui injecter l'outil déclaré précédemment ainsi qu'un template de prompt issu du Hub LangChain :

```python
agent_executor = AgentExecutor(
    agent=create_openai_functions_agent(
        _CHAT, _TOOLS, hub.pull("hwchase17/openai-functions-agent")
    ),
    tools=_TOOLS,
)
```

## Définir une fonction d'appel de l'agent

Afin de mieux décortiquer le comportement de l'agent, créons une fonction qui va nous permettre de poser des questions à l'agent et d'enregistrer les actions qu'il a effectuées.

```python
def ask_question(question):
    """Poser une question au modèle avec l'agent et ses outils."""
    response = None
    used_tools = []

    # Lance l'évaluation du prompt
    for chunk in agent_executor.stream({"input": question}):
        # Enregistrement des actions du LLM
        if "actions" in chunk:
            for action in chunk["actions"]:
                used_tools.append(action.tool)
                response = action.tool_input
        if "output" in chunk:
            response = chunk["output"]
    return response, used_tools
```

Cette fonction retourne un tuple contenant la réponse du modèle et la liste des outils utilisés.

## Appeler son agent

Enfin, il nous suffit d'appeler notre agent !

```python
print("""\n🕵️  Agent 🕵️\n""")
print("💬 Votre question : ")
answer, tools_list = ask_question(input())
print("🕵️  Réponse de l'agent : ")
print(answer)
print("\n🔨 Outils utilisés : ")
for tool in tools_list:
    print(f"    ◼ {tool}")
```

Lancez le script python directement avec la ligne de commande suivante :

```sh
python prompt_agent.py
```

Vous pouvez également aller sur votre interface Streamlit sur la page de l'agent.

## Intégrer un nouvel outil permettant de faire des recherches sur Wikipedia

Allez, à vous de jouer pour intégrer le prochain outil ci-dessous et l'utiliser.

```python
from langchain_community.tools import WikipediaQueryRun
from langchain_community.utilities import WikipediaAPIWrapper

# Nouvel outil à ajouter :
Tool.from_function(
    func=WikipediaQueryRun(api_wrapper=WikipediaAPIWrapper()).run,
    name="Wikipedia",
    description="Useful when you need an answer about encyclopedic general knowledge",
    handle_tool_error=True,
)
```

Vous vous en doutez, celui-ci va permettre d'aller chercher du contenu sur Wikipedia pour répondre à des questions d'actualité.

Exemple de questions :

* Elisabeth Borne est-elle toujours première ministre de la France ?
* Combien d'albums Michael Jackson a-t-il vendu ?

Une fois que vous avez terminé cette partie, vous pouvez passer aux [outils custom](./customs.md).
