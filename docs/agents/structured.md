Et pour finir, vous pouvez même créer des outils plus complexes à l'aide des `StructuredTools` !

Pour suivre cette partie, ouvrez les fichiers suivants :

* Prompt en mode Agent : `prompt_agent.py`
* Déclaration des outils custom : `tools.py`

## Déclarer son structured tool

Cette fois, nous allons nous amuser à récupérer les paroles de chansons via une API dédiée.

Celle-ci prend en paramètre le nom de l'artiste et le titre de la chanson.

Ajoutez le code suivant dans le fichier `tools.py` :

```python
from langchain_core.tools import BaseModel, Field, tool

class LyricsInput(BaseModel):
    """Input for the lyrics tool."""

    artist: str = Field(description="should be an artist name")
    title: str = Field(description="should be the title of their song")


def get_lyrics(artist: str, title: str):
    """Gets the lyrics of a song."""
    url = f"https://api.lyrics.ovh/v1/{artist}/{title}"
    response = requests.get(url, timeout=15)

    if response.status_code == 200:
        return response.json()["lyrics"]
    return None
```

## Enregistrer l'outil structuré

Comme tout à l'heure, nous allons ensuite enregistrer l'outil dans l'agent.

Ajoutez le code suivant dans `prompt_agent.py` :

```python
# On n'oublie pas d'ajouter StructuredTool dans les imports
from langchain.tools import StructuredTool, Tool

_TOOLS = [
    # Ajout d'un tool structuré
    StructuredTool.from_function(
        func=tools.get_lyrics,
        name="Lyrics",
        description="Useful when you need to get lyrics from an artist and title",
        handle_tool_error=True,
        args_schema=tools.LyricsInput,
    ),
]
```

## Tester l'outil structuré

Pour tester l'outil, relancez votre code et posez une question comme :

* Quelles sont les paroles du titre Killing in the Name de Rage Against the Machine ?
* Quelles sont les paroles du titre de Tryo intitulé L'Hymne de nos campagnes ?

Et ce en lançant le script python directement avec la ligne de commande suivante :

```sh
python prompt_agent.py
```

Vous pouvez également aller sur votre interface Streamlit sur la page de l'agent.

Et ... that's it for today! Si vous êtes motivés, vous pouvez vous amuser à implémenter des outils encore plus complexes 😇.
