Maintenant, passons aux choses sérieuses et créons nos propres outils !

Pour suivre cette partie, ouvrez les fichiers suivants :

* Prompt en mode Agent : `prompt_agent.py`
* Déclaration des outils custom : `tools.py`

## Créer des outils simples

La première approche la plus basique consiste à créer des outils à l'aide du décorateur `@tool`.

```python
import requests
from langchain_core.tools import tool

@tool
def get_pokemon_details(pokemon_name):
    """Get pokemon details by its pokemon name.

    Args:
        pokemon_name (str): The name of the pokemon to get details for.

    Returns:
        dict: A dictionary containing the details of the pokemon."""
    url = f"https://pokeapi.co/api/v2/pokemon/{pokemon_name.lower()}"
    response = requests.get(url, timeout=15)
    if response.status_code == 200:
        data = response.json()
        return {
            "name": data["name"],
            "height": data["height"],
            "weight": data["weight"],
            "base_experience": data["base_experience"],
            "abilities": [ability["ability"]["name"] for ability in data["abilities"]],
        }
    return None
```

Ici, notre outil va permettre de récupérer des informations sur un Pokémon via PokeAPI.

## Enregistrer l'outil custom

Reprenons le code vu dans le chapitre sur les agents et ajoutons l'outil à la liste.

Ajoutez le code suivant dans le fichier `prompt_agent.py` :

```python
# On n'oublie pas d'importer le package `tools` où on vient de déclarer la fonction.
import tools

_TOOLS = [
    # Ajouter l'outil dans le tableau de _TOOLS.
    Tool.from_function(
        func=tools.get_pokemon_details,
        name="PokemonDetails",
        description="Useful when you need to answer about Pokemon characteristics",
        handle_tool_error=True,
    ),
]
```

## Tester l'outil custom

Pour tester l'outil, relancez votre code et posez une question comme :

* Quelle est la taille de Pikachu ?
* Combien pèse Charizard ?

(Oui, oui, l'API ne dispose des Pokémon qu'en anglais...)

## Ajouter un outil pour connaître la localisation des Pokémon

Maintenant, ajoutez cet outil pour connaître la localisation des Pokémon.

Ajoutez le code suivant dans le fichier `tools.py` :

```python
@tool
def get_pokemon_locations(pokemon_name):
    """Get the locations of a pokemon.

    Args:
        pokemon_name (str): The name of the pokemon to get locations for.

    Returns:
        list: A list of locations of the pokemon."""
    url = f"https://pokeapi.co/api/v2/pokemon/{pokemon_name.lower()}/encounters"
    response = requests.get(url, timeout=15)

    if response.status_code == 200:
        data = response.json()
        return [location["location_area"]["name"] for location in data]
    return None
```

On vous laisse le soin d'ajouter l'outil dans la liste des `_TOOLS` du fichier `prompt_agent.py` avec les informations adéquates.


Vous pouvez ensuite faire quelques tests tels que :

* Où est localisé Pikachu ?
* Où trouver Moltres ?

(Moltres = Sulfura 😅)

Et ce en lançant le script python directement avec la ligne de commande suivante :

```sh
python prompt_agent.py
```

Vous pouvez également aller sur votre interface Streamlit sur la page de l'agent.

Une fois que vous avez terminé cette partie, vous pouvez passer aux [outils structurés](./structured.md).
