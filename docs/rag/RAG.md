## Introduction au concept de RAG

Retrouvez la documentation relative au RAG sur la [documentation Langchain](https://python.langchain.com/v0.2/docs/tutorials/rag/).

![alt text](image.png)

![alt text](image-1.png)

## Ingestion et vectorisation

Voici les étapes à mettre en œuvre pour cette section :

* Initialisation de la base de données vectorielle
* Ingestion des documents dans la BDD
    * Lire les documents PDF
    * Faire un embedding des PDF

Le fichier `ingest.py` a été préparé pour cette partie. Nous utiliserons Langchain pour nous faciliter la tâche sur ces traitements. Les imports des fonctionnalités nécessaires pour cela ont été conservés pour vous guider.

```python
from langchain_community.document_loaders import PyPDFDirectoryLoader
from util_API_OVH.embedding import OVHcloudAIEEmbeddings
from langchain_chroma import Chroma
```

Réferez vous à la [documentation Langchain](https://api.python.langchain.com/en/latest/langchain_api_reference.html) pour comprendre l'usage des différentes classes python et notamment des embeddings.

Les fonctions d'embedding OVH n'étant pas encore intégrées à Langchain, vous trouverez un snippet qui permet de prendre en charge celles-ci dans `util_API_OVH/embedding.py`.

L'impact est le suivant : il faudra modifier le paramètre `embedding_function` lors de l'initialisation de la base de données :

```python
embedding_function=OVHcloudAIEEmbeddings(api_key=os.environ.get("OVH_AI_ENDPOINTS_ACCESS_TOKEN", None))
```

Vous pouvez tester l'ingestion avec le document de votre choix, de préférence un PDF (les autres types de documents ne sont pas implémentés dans le corrigé). Un document exemple a été mis à disposition (rapport du GIEC).

Pour tester votre code :

```sh
python ingest.py
```

## Retrieving

Maintenant que la base de données est initialisée, il ne reste plus qu'à interroger celle-ci.

Le fichier `retrieving.py` a été préparé pour cette partie. Nous utiliserons Langchain pour nous faciliter la tâche sur ces traitements.

Cela va consister à utiliser les fonctions de recherche de la base de données en utilisant les retrievers.

L'objectif sera de faire une recherche sémantique par similarité pour rechercher les documents dans la base de données qui sont proches de la question posée.

[Documentation ChromaDB](https://python.langchain.com/v0.2/docs/integrations/vectorstores/chroma/#basic-example-using-the-docker-container)

Pour tester votre code :
```sh
python retrieving.py
```

## RAG : première version

Il ne reste plus qu'à assembler les pièces du puzzle.

Voici les étapes à mettre en œuvre pour cette section :

* Initialisation de la base de données vectorielle
* Initialisation du Chat
* Recherche sémantique/vectorielle pour identifier les sources
* Appel au LLM en passant en paramètre la question et le résultat de la recherche vectorielle

Le fichier `prompt_rag.py` a été préparé pour cette partie. Nous utiliserons toujours Langchain pour nous faciliter la tâche. Les imports des fonctionnalités nécessaires pour cela ont été conservés pour vous guider.

Pour tester votre code, vous pouvez aller sur la page `1-RAG 🔍` ou alors directement lancer le fichier Python :

```sh
python prompt_rag.py
```
