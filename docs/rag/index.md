Dans cette partie, nous commençerons par apprendre comment intéragir avec un LLM et nous irons jusqu'à la mise en place d'un RAG (Retrieval Augmented Generation).

Au programme :

* Appeler un LLM : Mixtral
* Une webapp pour interragir avec les LLM
* Mise en place d'un RAG :
    * Ingestion / vectorisation
    * Retrieving et recherche sémantique
    * Première version RAG

## Appeler un LLM : Mixtral

On va démarrer tranquillement en essayant d'intérragir avec une des API OVH.

Le fichier `prompt.py` a été préparé pour cet exercice.

Commencez par éditer votre fichier `.env` pour renseigner la clé d'API OVH :

```sh
OVH_AI_ENDPOINTS_ACCESS_TOKEN="<clé récupéré sur votre compte OVH>"
```

Les API OVH ne sont pas directement intégré à Langchain, mais elles sont compatibles avec l'implément OpenAI qu'il faut surcharger avec certains paramètres. Cela est déjà configuré pour l'exercice.

Prenez le temps de parcourir la documentation de l'API OVH pour observer et tester les modèles mis à disposition.
https://endpoints.ai.cloud.ovh.net/

L'objectif de cette étape sera de réussir à faire un appel à Mixtral et se connecter à son premier LLM.

Le code est annoté avec des indications sur les modifications attendues.

Exemple :

```python
# Code à compléter : pour appeler le modèle et générer la réponse ; cf. documentation de ChatOpenAI
```


Pour  tester :
```sh
python prompt.py
```

## Webapp streamlit

Pour cette section, on s'appuiyera sur la bibliothèque `streamlit`pour créer une IHM de test facilement et rapidement.

* [Documentation Streamlit](https://docs.streamlit.io/)
* [Streamlit gallery (exemples de composants streamlit)]( https://streamlit.io/gallery)

Le fichier `Home.py`a été préparé à cet effet. 
La structure est prêt et il est juste nécessaire de rajouter les composants graphiques pour interragir avec le LLM précédemment configuré.

Pour lancer l'application avec streamlit et tester :
```sh
streamlit run Home.py
```

