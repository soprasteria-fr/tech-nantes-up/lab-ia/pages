🧪 Lab IA 🧪
===========================

Bienvenue sur ce Lab IA !

## Démarrage

Pour démarrer, rendez-vous sur [l'environnement de développement web](https://hello.lab-ia.cloud-sp.eu/).

Cliquez sur le bouton "Générer l'environnement" puis attendez quelques secondes et cliquez sur votre lien d'environnement vscode pour accéder à votre espace de travail.

**Merci de traiter gentillement cet espace de travail personnel, les machines virtuelles restent partagées entre tous les participants.**

Pensez aussi à aller sur les [endpoints IA d'OVH](https://endpoints.ai.cloud.ovh.net/) pour récupérer votre jeton d'accès!

## Où trouver le code ?

Tout le code utilisé pour ce lab ce trouve sur le [GitLab de Sopra Steria](https://gitlab.com/soprasteria-fr/tech-nantes-up/lab-ia).

Si tu as besoin des corrections complètes, elles sont [disponibles ici](https://gitlab.com/soprasteria-fr/tech-nantes-up/lab-ia/codelab-corrige).

## Et si je suis bloqué ?

Demande aux animateurs de t'aider ou regarde la correction.
