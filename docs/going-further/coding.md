Psssst ! Bon, on vous a menti, il reste encore un peu de contenu pour les plus acharnés d'entre vous !

## Utiliser un LLM pour faire du développement

Vous avez entendu parler de GitHub Copilot ? On imagine que oui. Cela dit, il existe des alternatives avec des plugins Open Source disponibles sur VS Code.

Par exemple, dans votre environnement actuel, vous pouvez installer un plugin appelé [Continue](https://continue.dev/) qui permet de poser des questions à un LLM sur des tâches de développement et sur votre projet ouvert.

On vous propose de le configurer pour tester avec les endpoints de chez OVH !

1. Allez dans les extensions de VS Code (icône de briques).
2. Cherchez l'extension "Continue" et installez-la. Faites attention à bien la mettre à jour (la version doit être supérieure ou égale à `v0.8.39`). **Un rechargement de l'IDE peut être nécessaire.**
3. Cliquez sur l'icône du plugin à gauche (un cercle).
4. Cliquez sur "Local models" puis faite "Continue"
4. Un chat apparaît normalement dans votre fenêtre, cliquez sur la roue en bas pour ouvrir la configuration.
5. Ajoutez un nouveau modèle comme CodeLlama dans la section `models` comme suit :

```json
"models": [
    {
        "title": "CodeLlama-13b-OVH",
        "model": "CodeLlama-13b-Instruct-hf",
        "apiBase": "https://codellama-13b-instruct-hf.endpoints.kepler.ai.cloud.ovh.net/api/openai_compat/v1",
        "apiKey": "<VOTRE API KEY OVH>",
        "completionOptions": {
            "stream": true,
            "temperature": 0.1
        },
        "provider": "openai"
    }
]
```

6. Dans la liste déroulante sélectionnez votre nouveau model et posez vos premières questions dans le chat :
    * `Comment créer une application Flask en Python ?` : aide généraliste sur un sujet particulier
    * `@Terminal explique-moi ce qu'il se passe dans mon terminal` : demande d'expliquer le contenu du terminal actuellement ouvert
    * `@prompt_agent.py explique-moi ce que fait ce fichier` : demande des explications de code sur le fichier des agents
7. Il est même possible de lui faire générer les tests unitaires d'un fichier :
    * `/test @tools.py`
8. Copiez le contenu généré dans un fichier `tests.py`.
9. Installez pytest :
```bash
pip install pytest
```
10. Lancez les tests :
```bash
pytest tests.py
```

## Et si on veut de l'autocomplete ?

Pour faire de l'autocomplete, vous pouvez utiliser un plugin comme [Tabby](https://tabby.tabbyml.com/).

Récupèrez la clé d'API sur ce pastebin : [https://pastebin.com/KAvv7EYV](https://pastebin.com/SC2xUi2P).

1. Installez l'extension depuis le marketplace de VSCode.
2. Une fois l'extension installée, cliquez en bas à droite sur "Tabby" puis "Settings"
3. Entrez l'URL suivante dans la section "Endpoint": `https://tabby.ovh.di-ai.soprasteria.com/`
4. Lorsqu'on vous demande une clé d'API, collez celle fournie par les organisateurs du Lab

Et voilà, vous pouvez maintenant utiliser l'autocomplete intelligent de Tabby pour générer du code.

**Il est possible qu'il y ait quelques soucis de refresh une fois la clé insérée. Dans ce cas rafraichissez l'IDE.**
